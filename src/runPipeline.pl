#!/usr/bin/perl


=head1 Name

        runPipeline.pl

=head1 SYNOPSIS

Use:
        perl runPipeline.pl -commit e83fkc8 -project 'Xenbase/playground' -branch

=head1 ARGUMENTS

        =item commit
                commit version

        =item project
                GitLab Project

        =item branch
                GitLab Branch

        =item files
                File containing filetypes that trigger pipeline run

=head1 Author
        Joshua Fortriede
        Joshua.Fortriede@cchmc.org
        jdfortriede@gmail.com

=head1 Date
        Dec 9, 2018

=cut

use Getopt::Long;
use Pod::Usage;
use Data::Dumper;
use Cwd;
use config;

my $CWD = cwd();
print Dumper \%genome;

my $commit = '';
my $project = '';
my $branch = '';
my $files = '';
my $species = '';

GetOptions(
        'commit|c=s' => \$commit,
        'project|p=s' => \$project,
        'branch|b=s' => \$branch,
        'files|f=s' => \$files,
        'species|s=s' => \$species,
        'help|h' => \$HELP,
        'man' => \$MAN
) or pod2usage(2);

pod2usage(1) if $HELP;
pod2usage(-exitstatus => 0, -verbose => 2) if $MAN;

print join("\n","Arguments",@ARGV,"","");

open(FILES, $files);
while(<FILES>){
	chomp;
	next if $_=~ m/^#/;
	my ($file) = $_;
	
	$file =~ s/^\*/.*/;
	push(@checkFiles, $file);
}
my $checkFilesString = '^('.join("|",@checkFiles).'$)';
my $cmd = "git log --name-only -1 -U $commit --oneline | tail -n +2 | grep -iP '$checkFilesString' | wc -l";
my $wc = `$cmd`;
if($wc>0)	{	print "Run rest of pipeline\n";	}
else		{	print "Exit pipeline\n"; 
#exit;
	}

print "\n\n====================\n\n\n";

my $genomeGZ =  $genome{$project};
my $GFF = "$CWD/$gff{$project}";

print "perl $scriptDir/gffToGTF.pl -gff $GFF -exon all\n";

`perl $scriptDir/gffToGTF.pl -gff $GFF -exon all`;
`cp $genomeGZ ~/tmp/$commit.fa.gz`;
`gzip -d ~/tmp/$commit.fa.gz`;

my $genomeWidth = $commit."_80.fa";

print join("\n",$genomeGZ, $genomeWidth,$GFF,"");

#make a genome FASTA file formatted to width of 80 characters
print "perl $scriptDir/wrapFasta.pl -fasta ~/tmp/$commit.fa -width 80 -out ~/tmp/$genomeWidth\n"; 
`perl $scriptDir/wrapFasta.pl -fasta ~/tmp/$commit.fa -width 80 -out ~/tmp/$genomeWidth`; 
`perl $scriptDir/createGenomeIndex.pl -fasta ~/tmp/$genomeWidth`;
`perl $scriptDir/createNewFilesAfterGFFUpdate.pl -genome ~/tmp/$genomeWidth -gff $GFF`;
`perl $scriptDir/createMetadataFilesForGFF.pl -gff $GFF -species $species`;

`mv *.fa sequences`;
`rm *.index`;
`rm ~/tmp/$commit*`;



`git add .`;
`git add ./\*`;
`git add metadata/\*`;
`git add sequences/\*`;
`git add --all`;
`git commit -m "automated commit from CI pipeline [skip ci']"`;
`git push`;






